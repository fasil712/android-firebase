package com.androidcodegeeks.checkfirebase;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {
    EditText email,etname;
    Button btnInsert;
    DatabaseReference reference;
    User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        email = findViewById(R.id.email);
        etname = findViewById(R.id.name);
        btnInsert = findViewById(R.id.insert);
        reference = FirebaseDatabase.getInstance().getReference("User");
        user = new User();

        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addData();
                Toast.makeText(MainActivity.this, "Data is Inserted...",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void addData(){
        String name = etname.getText().toString().trim();
        String emails = email.getText().toString().trim();
        user = new User(name,emails);
        reference.push().setValue(user);
    }
}
